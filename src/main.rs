use std::{
    borrow::Cow, 
    collections::HashMap, 
    str::FromStr, 
    sync::Arc, time::{Duration, Instant}, 
    env
};

use chrono::{DateTime, Utc};
use reqwest::Client;
use colored::Colorize;
use dotenv::dotenv;
use ncurses::*;
use tokio::sync::{
    mpsc::{Receiver, Sender}, Mutex
};

mod structs;

use structs::{Timeline, Weather};

#[derive(PartialEq, PartialOrd)]
enum Print {
    Ncurses,
    Default,
    #[allow(unused)]
    Nothing
}

const WIDTH:         i32 = 10;
const HEIGHT:        i32 =  3;

const WINDOW_WIDTH:  i32 = 10;
const WINDOW_HEIGHT: i32 = 10;

//time from response, Timeline, time when it was asked
type TTI = (String, Timeline, Duration, Instant);
//          Timeline, time, location
type TTL = (Timeline, String, String);

// contains already asked things
struct Cache {
    asked: HashMap<String, TTI>
}

impl Cache { fn new() -> Cache { Cache { asked: HashMap::new() } } }

// for instance: rio de janeiro -> Rio De Janeiro
fn format_location(location: &str) -> String {
    location
        .split_whitespace()
        .map(|word| {
            let (fc, rest) = word.split_at(1);
            Cow::Owned(format!("{fc}{rest}", fc = fc.to_uppercase()))
        })
        .collect::<Vec<Cow<str>>>()
        .join(" ")
}

#[allow(unused)]
async fn print_weather(
    strct:    &Timeline, 
    time:     &String, 
    location: &String, 
    print:    &Print) -> std::io::Result<()> 
{
    let names = strct.values.get_names();
    let fields = strct.values.get_fields();
    let max_name_length = names.iter().map(|name| name.len()).max().unwrap_or(0);

    match print {
        Print::Default => { 
            println!(
                "Weather in {} at UTC: {}\nYou can enter another one or ^C",
                location.bright_cyan(),
                time.bright_green()
            ); 
        }
        Print::Ncurses => { 
            addstr(&format!("Weather in {location} at UTC: {time}\nYou can enter another one or ^C"));
            addstr("\n\nYou can enter another one or q");
        }
        _              => {}
    }
    for (index, (name, field)) in names.iter().zip(fields).enumerate() {
        if let Some(field) = field {
            let padded_name = format!("{:<width$}", format!("{name}:"), width = max_name_length + 1);
            match print {
                Print::Default => { 
                    let formatted_output = format!("{name} {field}", 
                        name = padded_name.bright_white().italic(), 
                        field = field.to_string().bright_cyan());
                    println!("{formatted_output}");
                }
                Print::Ncurses => { mvprintw(index as i32, 0, &format!("{padded_name} {field}")); }
                _              => {}
            }
        }
    } Ok(())
}

#[allow(unused)]
async fn get_weather(
    url:      String, 
    client:   Client, 
    location: String, 
    cache:    Option<&mut Cache>) -> Result<Option<TTL>, String> 
{
    let location = format_location(&location);

    // getting data from API
    match client.get(&url).header(reqwest::header::ACCEPT, "application/json").send().await {
        Ok(response) => {
            if response.status().is_success() {
                let response_text = response.text().await.unwrap_or_default();
                let weather: Weather = serde_json::from_str(&response_text).expect("Failed to parse JSON for Weather");
                let current_time = Utc::now();

                // getting nearest entry
                if let Some(nearest_entry) = weather.timelines.minutely.iter().min_by_key(|entry| {
                    let entry_time = DateTime::parse_from_rfc3339(&entry.time).unwrap();
                    let entry_duration = entry_time.signed_duration_since(current_time);
                    entry_duration.abs().to_std().unwrap() 
                }) {
                    // formatting to normal time like 15:25 PM
                    let formatted_time = DateTime::<Utc>::from_str(&nearest_entry.time)
                        // H -> hour, M -> minute, %p -> PM/AM
                        .map(|t| t.format("%H:%M %p").to_string())
                        .unwrap_or_else(|_| "N/A".to_string());

                    if let Some(cache) = cache {
                        cache.asked.insert(location.to_lowercase(), (formatted_time.clone(), nearest_entry.clone(), Duration::from_secs(5), Instant::now()));
                    } Ok(Some((nearest_entry.clone(), formatted_time, location)))
                } else { Err(format!("No forecast data available for {location}")) }
            } else { Err(format!("No forecast data available for {location}")) }
        }
        Err(e) => Err(format!("{e}")),
    }
}

fn draw_box(mut width: i32, height: i32, x: i32, y: i32) {
    width = (width as f32 * 2.5) as i32;
    mvhline(y, x, ACS_HLINE(), width);
    mvhline(y + height, x, ACS_HLINE(), width);
    mvvline(y, x, ACS_VLINE(), height);
    mvvline(y, x + width, ACS_VLINE(), height);
    mvaddch(y, x, ACS_ULCORNER());          
    mvaddch(y, x + width, ACS_URCORNER());  
    mvaddch(y + height, x, ACS_LLCORNER()); 
    mvaddch(y + height, x + width, ACS_LRCORNER()); 
}

fn is_cursor_inside_box(
    x: i32, y: i32, 
    w: i32, h: i32, 
    box_x: i32, box_y: i32, 
    box_w: i32, box_h: i32) -> bool 
{
    x + w >= box_x && x <= box_x + box_w && y + h >= box_y && y <= box_y + box_h
}

#[allow(unused, non_snake_case)]
async fn ncurses(
    API_KEY:   String, 
    client:    &Client, 
    tx:        Sender<Option<TTL>>, 
    mut rx:    Receiver<Option<TTL>>, 
    mut cache: Cache) -> std::io::Result<()> 
{
    let client = Arc::new(Mutex::new(Client::new()));
    let tx     = Arc::new(Mutex::new(tx));
    let cache  = Arc::new(Mutex::new(cache));
    // let's keep it as new york for now 
    let input  = Arc::new(Mutex::new("new york".to_owned()));
    let url    = Arc::new(Mutex::new(format!(
        "https://api.tomorrow.io/v4/weather/forecast?location={request}&apikey={API_KEY}",
        request = input.lock().await.to_lowercase().replace(" ", "%20"),
    )));

    fn get_ctui(
        client: &Arc<Mutex<Client>>,
        tx:     &Arc<Mutex<Sender<Option<TTL>>>>,
        url:    &Arc<Mutex<String>>,
        input:  &Arc<Mutex<String>>,
        cache:  &Arc<Mutex<Cache>>
    ) -> (Arc<Mutex<Client>>, Arc<Mutex<Sender<Option<TTL>>>>, Arc<Mutex<String>>, Arc<Mutex<String>>, Arc<Mutex<Cache>>) {
        (client.clone(), tx.clone(), url.clone(), input.clone(), cache.clone())
    }

    // client, tx, url, input -> CTUI
    async fn process_weather_request(
        client: Arc<Mutex<Client>>,
        tx:     Arc<Mutex<Sender<Option<TTL>>>>,
        url:    Arc<Mutex<String>>,
        input:  Arc<Mutex<String>>,
        cache:  Arc<Mutex<Cache>>
    ) {
        let client    = client.lock().await;
        let mut cache = cache.lock().await;
        let url       = url.lock().await;
        let input     = input.lock().await;

        addstr("Sended");
        if let Ok(Some(result)) = get_weather(
            url.to_owned(),
            client.to_owned(),
            input.to_owned(),
            Some(&mut cache)).await {
            tx.lock().await.send(Some(result)).await.expect("Failed to send result through channel");
        }
    }

    let (client_clone, tx_clone, url_clone, input_clone, cache_clone) = get_ctui(&client, &tx, &url, &input, &cache);
    if !cache.lock().await.asked.contains_key(&*input_clone.lock().await) {
        tokio::spawn(async move {
            process_weather_request(client_clone, tx_clone, url_clone, input_clone, cache_clone).await;
        });
    } else {
        let result = cache_clone.lock().await.asked.get(&*input_clone.lock().await).unwrap().clone();
        tx_clone.lock().await.send(Some((result.1.clone(), result.0.clone(), input_clone.lock().await.to_owned())))
            .await.expect("Failed to send result through channel");
    }

    initscr();
    raw();
    keypad(stdscr(), true);
    noecho();
    curs_set(CURSOR_VISIBILITY::CURSOR_VISIBLE);

    let mut max_x = 0;
    let mut max_y = 0;
    getmaxyx(stdscr(), &mut max_y, &mut max_x);

    #[allow(non_snake_case)]
    let WINDOW_X_CENTER = (COLS() - WINDOW_WIDTH) / 2;
    #[allow(non_snake_case)]
    let WINDOW_Y_CENTER  = (LINES() - WINDOW_HEIGHT) / 2;
    let mut y = (max_y - HEIGHT) / 2;
    let mut x = (max_x - WIDTH) / 2;
    mv(y, x);
    refresh();

    timeout(100);
    napms(100);

    draw_box(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_X_CENTER, WINDOW_Y_CENTER);
    mvprintw(WINDOW_Y_CENTER + 1, WINDOW_X_CENTER + 2, "weather"); 
    refresh();

    let mut weather: Option<TTL> = None;
    loop {
        let ch = getch();

        match ch {
            k if k == KEY_LEFT  || k == b'h' as i32 => { x -= 1; }
            k if k == KEY_RIGHT || k == b'l' as i32 => { x += 1; } 
            k if k == KEY_UP    || k == b'k' as i32 => { y -= 1; } 
            k if k == KEY_DOWN  || k == b'j' as i32 => { y += 1; }
            k if k == 10        || k == b'm' as i32 => {
                if is_cursor_inside_box(x, y, WIDTH, HEIGHT, WINDOW_X_CENTER, WINDOW_Y_CENTER, WINDOW_WIDTH, WINDOW_HEIGHT) {
                    clear();
                    if let Some(data) = rx.recv().await {
                        weather = data;
                    };
                    let (client_clone, tx_clone, url_clone, input_clone, cache_clone) = get_ctui(&client, &tx, &url, &input, &cache);
                    if !cache.lock().await.asked.contains_key(&*input_clone.lock().await) {
                        process_weather_request(client_clone, tx_clone, url_clone, input_clone, cache_clone).await;
                    } else {
                        let result = cache_clone.lock().await.asked.get(&*input_clone.lock().await).unwrap().clone();
                        tx_clone.lock().await.send(Some((result.1.clone(), result.0.clone(), input_clone.lock().await.to_owned())))
                            .await.expect("Failed to send result through channel");
                    }
                    if let Some(ref weather) = weather { 
                        print_weather(&weather.clone().0, &weather.clone().1, &weather.clone().2, &Print::Ncurses).await.ok(); 
                    } else { addstr("Something went wrong"); }
                }
            }
            // TODO
            k if k == b'i' as i32 => {
            }
            k if k == b'b' as i32 => {
                clear();
                draw_box(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_X_CENTER, WINDOW_Y_CENTER);
                mvprintw(WINDOW_Y_CENTER + 1, WINDOW_X_CENTER + 2, "weather");
                tokio::time::sleep(Duration::from_millis(1000));
            }
            k if k == b'q' as i32 => break,
            _ => (),
        }
        
        mv(y, x);
        refresh();
        tokio::time::sleep(Duration::from_millis(100));
    }
    endwin();
    Ok(())
}

#[allow(unused, non_snake_case)]
async fn default(API_KEY: String, client: &Client) -> std::io::Result<()> {
    println!("{}\n{}, which stands for \"{}\".\n{}",
        "Welcome to the my ultimate application called: ".bright_cyan().bold(),
        "GWFANCI".bright_white().bold(),
        "Get Weather From Any City Instantly".bright_green().italic(),
        "^C to exit".strikethrough());

    let mut cache = Cache::new();
    loop {
        let mut input = String::new();
        match std::io::stdin().read_line(&mut input) {
            Ok(0) => { eprintln!("No input"); break; }
            Ok(_) => {
                if !input.is_empty() { 
                    let url = format!("https://api.tomorrow.io/v4/weather/forecast?location={request}&apikey={API_KEY}", 
                                  request = input.trim().to_lowercase().replace(" ", "%20"));

                    println!("Working on {location}..", location = format_location(&input));
                    match get_weather(url, client.clone(), input, Some(&mut cache)).await {
                        Ok(Some(ok)) => {
                            print_weather(&ok.clone().0, &ok.clone().1, &ok.clone().2, &Print::Default).await.ok(); 
                        }
                        Err(e) => eprintln!("{e}"),
                        _            => {}
                    }
                } else { println!("Stop doing shit just enter a city name"); }
            } Err(e) => { eprintln!("Error reading from stdin: {e}"); break; }
        }
    } Ok(())
}

// TODO: get rid of tomorrow api and hook data myself && 
// 1. Implement a feature allowing people that uses ncurses version 
// to enter the city of their choice, not only a new york xd).
// 
// 2. Implement a feature allowing check if data from response in Cache structure is
// up-to-date, and if it's not update with a new information if there's a need 
// from user
#[tokio::main]
async fn main() -> std::io::Result<()> {
    let exe_path = env::current_exe().expect("Failed to get current executable path");
    let project_directory = exe_path.parent().expect("Failed to get project directory");
    env::set_current_dir(&project_directory).expect("Failed to set current directory");
    dotenv().ok();

    #[allow(non_snake_case)]
    // tomorrow.io api key from https://docs.tomorrow.io/reference/welcome
    let API_KEY: &str = &std::env::var("TOMORROW_IO_API_KEY").expect("TOMORROW_IO_API_KEY must be set");
    #[allow(unused)]
    let (mut tx, mut rx) = tokio::sync::mpsc::channel::<Option<TTL>>(1);
    let client = Client::new();
    let cache = Cache::new();

    let args: Vec<String> = env::args().collect::<Vec<String>>();
    if let Some(arg) = args.get(1) {
        match arg.as_str() {
            "ncurses" | "ui" => ncurses(API_KEY.to_owned(), &client, tx, rx, cache).await.expect("Failed to run ncurses"),
            "help"    | "h"  => println!("getw ncurses or getw ui runs ncurses version\ngetw or getw default(getw def) runs default version"), 
            _                => default(API_KEY.to_owned(), &client).await?
        }
    } else { default(API_KEY.to_owned(), &client).await? }
    Ok(()) 
}

