use serde::{Deserialize, Serialize};

macro_rules! pubstruct {
    ($struct_name:ident { $($field_name:ident : $field_type:ty),* }) => {
        #[derive(Clone, Debug, Default, Serialize, Deserialize)]
        pub(crate) struct $struct_name {
            $(pub(crate) $field_name: $field_type),*,
        }
    };
}

pubstruct!(Timeline {
    time: String,
    values: WeatherValues
});

pubstruct!(Timelines {
    minutely: Vec<Timeline>
});

pubstruct!(Weather {
    timelines: Timelines
});

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct WeatherValues {
    #[serde(rename = "cloubBase")]
    pub cloud_base: Option<f64>,
    #[serde(rename = "cloudCeiling")]
    pub cloud_ceiling: Option<f64>,
    #[serde(rename = "temperature")]
    pub temperature: Option<f64>,
    #[serde(rename = "temperatureApparent")]
    pub temperature_apparent: Option<f64>,
    #[serde(rename = "temperatureAvg")]
    pub temperature_avg: Option<f64>,
    #[serde(rename = "cloudCover")]
    pub cloud_cover: Option<f64>,
    #[serde(rename = "dewPoint")]
    pub dew_point: Option<f64>,
    #[serde(rename = "freezingRainIntensity")]
    pub freezing_rain_intensity: Option<f64>,
    pub humidity: Option<f64>,
    #[serde(rename = "precipitationProbability")]
    pub precipitation_probability: Option<f64>,
    #[serde(rename = "pressureSurfaceLevel")]
    pub pressure_surface_level: Option<f64>,
    #[serde(rename = "rainIntensity")]
    pub rain_intensity: Option<f64>,
    #[serde(rename = "sleetIntensity")]
    pub sleet_intensity: Option<f64>,
    #[serde(rename = "snowIntensity")]
    pub snow_intensity: Option<f64>,
    #[serde(rename = "uvHealthConcern")]
    pub uv_health_concern: Option<f64>,
    #[serde(rename = "uvIndex")]
    pub uv_index: Option<f64>,
    pub visibility: Option<f64>,
    #[serde(rename = "weatherCode")]
    pub weather_code: Option<f64>,
    #[serde(rename = "windDirection")]
    pub wind_direction: Option<f64>,
    #[serde(rename = "windGust")]
    pub wind_gust: Option<f64>,
    #[serde(rename = "windSpeed")]
    pub wind_speed: Option<f64>,
}

impl WeatherValues {
    pub fn get_fields(&self) -> Vec<Option<f64>> {
        vec![
            self.cloud_base,
            self.cloud_ceiling,
            self.temperature,
            self.temperature_apparent,
            self.temperature_avg,
            self.cloud_cover,
            self.dew_point,
            self.freezing_rain_intensity,
            self.humidity,
            self.precipitation_probability,
            self.pressure_surface_level,
            self.rain_intensity,
            self.sleet_intensity,
            self.snow_intensity,
            self.uv_health_concern,
            self.uv_index,
            self.visibility,
            self.weather_code,
            self.wind_direction,
            self.wind_gust,
            self.wind_speed,
        ]
    }

    pub fn get_names(&self) -> Vec<String> {
        vec![
            "Cloud base".to_owned(),
            "Cloud ceiling".to_owned(),
            "Temperature".to_owned(),
            "Temperature apparent".to_owned(),
            "Temperature average".to_owned(),
            "Cloud cover".to_owned(),
            "Dew point".to_owned(),
            "Freezing rain intensity".to_owned(),
            "Humidity".to_owned(),
            "Precipitation probability".to_owned(),
            "Pressure surface level".to_owned(),
            "Rain intensity".to_owned(),
            "Sleet intensity".to_owned(),
            "Snow intensity".to_owned(),
            "Uv health_concern".to_owned(),
            "Uv index".to_owned(),
            "Visibility".to_owned(),
            "Weather code".to_owned(),
            "Wind direction".to_owned(),
            "Wind gust".to_owned(),
            "Wind speed".to_owned(),
        ]
    }
}
