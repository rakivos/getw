# PROJECT IS UNFINISHED!

# DON'T HAVE ANY EXPECTATIONS!

##### To run default(cli) mode just type getw or getw def
##### To run ui(ncurses) mode just type getw ui or getw ncurses
#### To add getw to your PATH run ./addtopath.sh script (working only on linux)

##### ncurses version isn't fully implemented, so you can NOT enter the city of your choice and it's always New York as you can see in the example 

##### To make it work just replace YOUR_API_KEY in .env file with your actual API_KEY from [Tomorrow.io API](https://docs.tomorrow.io/reference/welcome) and type 
```shell
$ cargo run --release
```
or
```shell
$ chmod +x ./addtopath.sh
$ ./addtopath.sh 
$ getw # to run in default mode
$ getw ui # to run in ui mode
```

# Preview of default(cli) version: 
![Preview](./assets/def.png)

# Previews of ui(ncurses) version: 
![Preview](./assets/ui.png)
