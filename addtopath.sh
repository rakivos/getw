#!/bin/bash

cargo build --release

CURRENT_DIR="$(pwd)"
APP_PATH="$CURRENT_DIR/target/release"

if [ -f .env ]; then
  source .env
fi

# setting up getw path
if grep -q "export PATH=\$PATH:$APP_PATH" ~/.bashrc; then
  echo "getw already added to PATH"
else
  sed -i "/export PATH=\$PATH:$APP_PATH/d" ~/.bashrc

  echo "export PATH=\$PATH:$APP_PATH" >> ~/.bashrc
  export PATH=$PATH:$APP_PATH
  echo "getw successfully added to PATH"
fi

# setting up api_key 
if [ -n "$TOMORROW_IO_API_KEY" ]; then
  if grep -q "export TOMORROW_IO_API_KEY=$TOMORROW_IO_API_KEY" ~/.bashrc; then
    echo "TOMORROW_IO_API_KEY already added to PATH"
  else
    sed -i "/export TOMORROW_IO_API_KEY=/d" ~/.bashrc

    echo "export TOMORROW_IO_API_KEY=$TOMORROW_IO_API_KEY" >> ~/.bashrc
    echo "TOMORROW_IO_API_KEY successfully added to .env"
  fi
else
  echo "TOMORROW_IO_API_KEY not set in .env"
fi

# updating .bashrc
source ~/.bashrc
